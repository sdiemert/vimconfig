# Simon's Vim Configuration

I'm tired of repeating the same Vim configuration on multiple VMs... this should automate it for me.

## Pre-Reqs

* Running some recent version of Ubuntu (relies on `apt install`)
* Git is installed (otherwise how did you clone this repo?)

## Usage

Clone the repo: `git clone https://gitlab.com/sdiemert/vimconfig.git`.

Move into the directory: `cd vimconfig`.

Run the installer script: `sudo sh ./install.sh`

Tell Vim to install plugins from its internal commandline: 

* `vi test.txt`
* `:PluginInstall`

