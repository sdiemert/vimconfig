
# Install vim if not already installed
sudo apt install vim -y

# Clone the Vundle utility
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Copy contents of .vimrc to ~/.vimrc
cp ./.vimrc ~/.vimrc

# Print a helpful message to complete install of packages
echo 'All done! Run :PluginInstall in vim to install Vundle packages'
